import { destroy, getMessageID, setMessageID, SizeChunker, waitForData, write } from "./utils";
import * as Crypto from "crypto";

import { encrypt, decrypt } from 'eciesjs'
import { randomBytes } from "crypto";
import { utils, sign, verify } from "@noble/secp256k1";
import { Readable } from "stream";
import nacl from "tweetnacl";

export function genIntro(): Buffer {
    let t = Buffer.alloc(7)
    t.writeIntBE(0, 0, 2)
    t.write("P2PEM", 2, "utf8")
    console.log("sending intro ")
    return t
}

export function parseIntro(b: Buffer): boolean {
    console.log("received intro: ", b.toString("utf8", 2))
    return (b.readIntBE(0, 2) != 0 || b.toString("utf8", 2) !== "P2PEM")
}

export async function sendCapabilities(write: (d: any) => Promise<unknown>, capabilities: string[]) {
    //cap primer
    const capLength = capabilities.length
    let t = setMessageID(Buffer.alloc(4), 5)
    t.writeUIntBE(capLength, 2, 2)

    await write(t)

    for (let i = 0; i < capLength; i++) {
        let cap = capabilities[i]
        t = setMessageID(Buffer.alloc(2 + 1 + cap.length), 6)
        t.writeIntBE(cap.length, 2, 1)
        t.write(cap, 3, "utf8")
        await write(t);
    }

}

export async function receiveCapabilities(waitForData: waitForData, destroy: any): Promise<string[]> {

    const capPrimer = await waitForData();
    if (getMessageID(capPrimer) != 5) destroy("Invalid cap primer message ID")

    const capLength = capPrimer.readUIntBE(2, 2)

    if (capLength > 1024 || capLength < 0) destroy("Invalid capability primer")

    const clientCaps = []
    for (let i = 0; i < capLength; i++) {
        let d = await waitForData()
        if (getMessageID(d) != 6) destroy("Invalid cap message ID")
        const capLength = d.readUIntBE(2, 1)
        const recvCap = d.toString("utf8", 3)
        if (recvCap.length != capLength) {
            console.log(d)
            destroy(`Received capability wrong length ${capLength} - ${recvCap}`)
        }
        clientCaps.push(recvCap)
    }
    return clientCaps
}

export async function sendPubKey(write: write, pubKey: Buffer) {
    let t = setMessageID(Buffer.alloc(pubKey.byteLength + 2), 2)
    pubKey.copy(t, 2)
    await write(t)
}


export async function receivePubKey(waitForData: waitForData, destroy: destroy) {

    let data = await waitForData();
    if (getMessageID(data) !== 2) destroy("Expected pubKey message")
    // const serverPubKey = data.toString("binary", 2)
    return data.subarray(2)

}

export async function sendEncCheck(write: write, waitForData: waitForData, destroy: destroy, clientPubKey: Buffer) {
    const byteCount = Crypto.randomInt(1_024, (65_536 - 97))

    const bytes = Crypto.randomBytes(byteCount)
    const message = setMessageID(Buffer.alloc(byteCount + 97 + 2), 3)
    const enc = encrypt(clientPubKey, bytes)
    enc.copy(message, 2)

    await write(message)

    const data = await waitForData();

    if (getMessageID(data) !== 4) destroy("Expected message ID 4")

    if (Buffer.compare(bytes, data.subarray(2)) !== 0) destroy("Encryption ID check buffer content mismatch")

    await sendConfirmation(write)

}

export async function receiveEncCheck(write: write, waitForData: waitForData, destroy: destroy, privKey: Buffer) {
    let d = await waitForData();
    if (getMessageID(d) !== 3) destroy("Expected message ID 3")

    const data = decrypt(privKey, d.subarray(2))

    let msg = setMessageID(Buffer.alloc(data.length + 2), 4)
    data.copy(msg, 2)

    await write(msg)

    d = await waitForData();

    if (getMessageID(d) !== 1) destroy("Expected message ID 1")

}


export async function sendEncCheckED25519(write: write, waitForData: waitForData, destroy: destroy, clientPubKey: Buffer) {
    const byteCount = Crypto.randomInt(1_024, (65_536 - 97))

    const bytes = Crypto.randomBytes(byteCount)
    const message = setMessageID(Buffer.alloc(byteCount + 97 + 2), 3)
    //const enc = encrypt(clientPubKey, bytes)
    const enc = Buffer.from(nacl.box.after(message, clientPubKey.subarray(clientPubKey.length - 1, clientPubKey.length), clientPubKey.subarray(0, clientPubKey.length - 1)))
    enc.copy(message, 2)

    await write(message)

    const data = await waitForData();

    if (getMessageID(data) !== 4) destroy("Expected message ID 4")

    if (Buffer.compare(bytes, data.subarray(2)) !== 0) destroy("Encryption ID check buffer content mismatch")

    await sendConfirmation(write)

}

export async function receiveEncCheckED25519(write: write, waitForData: waitForData, destroy: destroy, privKey: Buffer) {
    let d = await waitForData();
    if (getMessageID(d) !== 3) destroy("Expected message ID 3")

    const data = nacl.box.open.after(d.subarray(2), privKey.subarray(privKey.length - 1, privKey.length), privKey.subarray(0, privKey.length - 1))
    if (!data) { destroy("enc check no data"); return }
    let msg = setMessageID(Buffer.alloc(data.length + 2), 4)
    Buffer.from(data).copy(msg, 2)

    await write(msg)

    d = await waitForData();

    if (getMessageID(d) !== 1) destroy("Expected message ID 1")

}


export async function genAndSendSymKey(write: write, waitForData: waitForData, destroy: destroy, privKey: Buffer, clientPubKey: Buffer) {


    const key = randomBytes(32)
    const iv = randomBytes(16)
    const fullKey = Buffer.alloc(key.length + iv.length)
    key.copy(fullKey, 0)
    iv.copy(fullKey, 32)

    const sig = Buffer.from(await sign(await utils.sha256(fullKey), privKey)) // 70 bytes

    const encKey = encrypt(clientPubKey, fullKey) //145 bytes

    let t = setMessageID(Buffer.alloc(2 + 2 + encKey.length + 2 + sig.length), 7)
    t.writeIntBE(encKey.length, 2, 2)
    encKey.copy(t, 4)
    t.writeIntBE(sig.length, encKey.length + 4, 2)
    sig.copy(t, encKey.length + 6)

    console.log({
        keyLength: encKey.length, sigLength: sig.length,
        kfb: encKey.at(0), klb: encKey.at(-1),
        sfb: sig.at(0), slb: sig.at(-1)
    })



    await write(t)


    if (getMessageID(await waitForData()) !== 1) destroy("expected confirmation of key validity")
    return { key, iv }

}

export async function receiveSymKey(waitForData: waitForData, destroy: destroy, write: write, privKey: Buffer, serverPubKey: Buffer) {

    let d = await waitForData()

    if (getMessageID(d) !== 7) destroy("expected ID 7")

    const keyLength = d.readIntBE(2, 2)
    const encKey = d.subarray(4, keyLength + 4)
    const sigLength = d.readIntBE(keyLength + 4, 2)
    const sig = d.subarray(keyLength + 6, keyLength + 6 + sigLength)

    console.log({
        keyLength, sigLength,
        kfb: encKey.at(0), klb: encKey.at(-1),
        sfb: sig.at(0), slb: sig.at(-1)
    })

    const fullKey = decrypt(privKey, encKey)

    if (!verify(sig, await utils.sha256(fullKey), serverPubKey)) destroy("Invalid signature for encrypted symKey")
    await sendConfirmation(write)

    const key = fullKey.subarray(0, 32)
    const iv = fullKey.subarray(32)

    return { key, iv }

}

export async function sendConfirmation(write: write) {
    await write(setMessageID(Buffer.alloc(2), 1))
}


export async function sendMsgReq(write: write, waitForData: waitForData, capability: string, size: bigint): Promise<Boolean> {
    let m = setMessageID(Buffer.alloc(2 + 1 + capability.length + 8), 8)
    m.writeUIntBE(capability.length, 2, 1)
    m.write(capability, 3, "utf8")
    m.writeBigUInt64BE(size, capability.length + 3)
    // m.writeUBigIntBE(size, capability.length + 2, 8)
    await write(m)
    const res = await waitForData()
    if (getMessageID(res) === 9 && res.readIntBE(2, 1) === 1) return true;
    return false
}

export async function sendMsg(write: write, waitForData: waitForData, data: Readable) {
    const ckr = new SizeChunker({
        chunkSize: 1_000_000,
        flushTail: true
    })
    data.pipe(ckr)

    for await (const chunk of ckr) {
        const data: Buffer = chunk.data
        let m = setMessageID(Buffer.alloc(2 + 8), 10)
        m.writeBigUInt64BE(BigInt(data.length), 2)
        const hash = Buffer.from(await utils.sha256(data))
        // hash.copy(m, 10)
        // data.copy(m, m.length)
        m = Buffer.concat([m, hash, data])
        let i = 0;
        do {
            await write(m)
            const res = await waitForData()
            if (getMessageID(res) !== 11) console.warn("Chunk ack bad ID");
            if (res.at(2) === 1) i = 4;
            i++
        } while (i < 3);

    }
    console.log("done")

}