import { Socket } from "net";
import { createConnection } from "net"
import { waitForDataUB, writeUB, destroyUB, setMessageID, waitForEncDataUB, writeEncUB, msgListenerUB, registerRecvBuffer, } from "./utils";
import { genIntro, parseIntro, receiveCapabilities, receiveEncCheck, receivePubKey, receiveSymKey, sendCapabilities, sendEncCheck, sendMsg, sendMsgReq, sendPubKey } from "./messages";
import { getPublicKey, utils } from "@noble/secp256k1"
import { createCipheriv, createDecipheriv } from "crypto";
import { createReadStream } from "fs";
import { Readable } from "stream";


export async function connect(): Promise<Socket> {
    const sock = createConnection({ host: "localhost", port: 30522 })
    return await new Promise(r => {
        sock.on("connect", () => r(sock))
    })
}

export type Context = {
    conn: Socket
} & Record<any, any>


export const capabilities = [
    "text:0.0.1",
    "sha256:0.0.1",
    "aes-256-ctr:0.0.1",
    "secp256k1:0.0.1"
]

async function main() {

    const conn = await connect();
    registerRecvBuffer(conn)
    conn.setKeepAlive()
    conn.setNoDelay(true)

    conn.on("error", (err) => {
        console.error(err)
    })
    conn.on("end", () => {
        throw new Error(`Stream closed - closing client`)
    })

    // @ts-ignore
    conn._writableState.highWaterMark = 1;
    // @ts-ignore
    conn._readableState.highWaterMark = 1;

    const destroy = destroyUB.bind(undefined, conn)
    const write = writeUB.bind(undefined, conn)
    const waitForData = waitForDataUB.bind(undefined, conn)

    await write(genIntro())

    console.log("written intro")

    if (parseIntro(await waitForData())) destroy("Invalid intro")

    console.log("intro done")

    await sendCapabilities(write, capabilities)

    console.log("sent capabilities: ", capabilities)

    const serverCaps = await receiveCapabilities(waitForData, destroy)
    console.log("server capabilities: ", serverCaps)

    // agreement.
    await write(setMessageID(Buffer.alloc(2), 1))


    const serverPubKey = await receivePubKey(waitForData, destroy)

    console.log("received pubkey ", serverPubKey)
    // console.log("ServerPubKey: ", serverPubKey)

    const privKey = Buffer.from(utils.randomPrivateKey())
    const pubKey = Buffer.from(getPublicKey(privKey))

    // console.log("pubKey: ", pubKey)

    await sendPubKey(write, pubKey);

    console.log("sent pubkey")

    await receiveEncCheck(write, waitForData, destroy, privKey)

    await sendEncCheck(write, waitForData, destroy, serverPubKey)

    const { key, iv } = await receiveSymKey(waitForData, destroy, write, privKey, serverPubKey)
    console.log("received encryption key", key)
    console.log("received IV", iv)

    let cipher = createCipheriv("aes-256-ctr", key, iv)
    let decipher = createDecipheriv("aes-256-ctr", key, iv)

    const writeEnc = writeEncUB.bind(undefined, conn, cipher)

    const waitForEncData = waitForEncDataUB.bind(undefined, conn, decipher)

    console.log("done")

    // await writeEnc(genIntro())

    // console.log("p2pem:", (await waitForEncData()).toString("utf8"))

    let state = { s: "waiting" }
    // @ts-ignore
    const msgListener = msgListenerUB.bind(undefined, writeEnc, waitForEncData, destroy, decipher, state, capabilities)
    // now we event-emitter bind to the socket to allow for unprompted comms
    conn.on("data", async (d) => {
        //@ts-ignore
        await msgListener(d)
    })

    state.s = "transmitting"
    const data = "Hello, World!"
    const reqStatus = await sendMsgReq(writeEnc, waitForEncData, "text:0.0.1", BigInt(data.length));
    if (reqStatus) {
        console.log("sending message ", data)
        const bData = Readable.from(Buffer.from(data, "utf8"))
        // const bData = createReadStream("./llama.png")
        await sendMsg(writeEnc, waitForEncData, bData)
    } else {
        console.log("validation failed");
    }

    console.log("done")
}

main()

