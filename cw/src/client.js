"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.capabilities = exports.connect = void 0;
const net_1 = require("net");
const utils_1 = require("./utils");
const messages_1 = require("./messages");
const secp256k1_1 = require("@noble/secp256k1");
const crypto_1 = require("crypto");
const stream_1 = require("stream");
async function connect() {
    const sock = (0, net_1.createConnection)({ host: "localhost", port: 30522 });
    return await new Promise(r => {
        sock.on("connect", () => r(sock));
    });
}
exports.connect = connect;
exports.capabilities = [
    "text:0.0.1",
    "sha256:0.0.1",
    "aes-256-ctr:0.0.1",
    "secp256k1:0.0.1"
];
async function main() {
    const conn = await connect();
    (0, utils_1.registerRecvBuffer)(conn);
    conn.setKeepAlive();
    conn.setNoDelay(true);
    conn.on("error", (err) => {
        console.error(err);
    });
    conn.on("end", () => {
        throw new Error(`Stream closed`);
    });
    // @ts-ignore
    conn._writableState.highWaterMark = 1;
    // @ts-ignore
    conn._readableState.highWaterMark = 1;
    const destroy = utils_1.destroyUB.bind(undefined, conn);
    const write = utils_1.writeUB.bind(undefined, conn);
    const waitForData = utils_1.waitForDataUB.bind(undefined, conn);
    await write((0, messages_1.genIntro)());
    console.log("written intro");
    if ((0, messages_1.parseIntro)(await waitForData()))
        destroy("Invalid intro");
    console.log("intro done");
    await (0, messages_1.sendCapabilities)(write, exports.capabilities);
    console.log("sent capabilities: ", exports.capabilities);
    const serverCaps = await (0, messages_1.receiveCapabilities)(waitForData, destroy);
    console.log("server capabilities: ", serverCaps);
    // agreement.
    await write((0, utils_1.setMessageID)(Buffer.alloc(2), 1));
    const serverPubKey = await (0, messages_1.receivePubKey)(waitForData, destroy);
    console.log("received pubkey ", serverPubKey);
    // console.log("ServerPubKey: ", serverPubKey)
    const privKey = Buffer.from(secp256k1_1.utils.randomPrivateKey());
    const pubKey = Buffer.from((0, secp256k1_1.getPublicKey)(privKey));
    // console.log("pubKey: ", pubKey)
    await (0, messages_1.sendPubKey)(write, pubKey);
    console.log("sent pubkey");
    await (0, messages_1.receiveEncCheck)(write, waitForData, destroy, privKey);
    await (0, messages_1.sendEncCheck)(write, waitForData, destroy, serverPubKey);
    const { key, iv } = await (0, messages_1.receiveSymKey)(waitForData, destroy, write, privKey, serverPubKey);
    console.log("received encryption key");
    let cipher = (0, crypto_1.createCipheriv)("aes-256-ctr", key, iv);
    let decipher = (0, crypto_1.createDecipheriv)("aes-256-ctr", key, iv);
    const writeEnc = utils_1.writeEncUB.bind(undefined, conn, cipher);
    const waitForEncData = utils_1.waitForEncDataUB.bind(undefined, conn, decipher);
    console.log("done");
    // await writeEnc(genIntro())
    // console.log("p2pem:", (await waitForEncData()).toString("utf8"))
    let state = { s: "waiting" };
    // @ts-ignore
    const msgListener = utils_1.msgListenerUB.bind(undefined, writeEnc, waitForEncData, destroy, decipher, state, exports.capabilities);
    // now we event-emitter bind to the socket to allow for unprompted comms
    conn.on("data", async (d) => {
        //@ts-ignore
        await msgListener(d);
    });
    state.s = "transmitting";
    const data = "Hello, World!";
    const reqStatus = await (0, messages_1.sendMsgReq)(writeEnc, waitForEncData, "text:0.0.1", BigInt(data.length));
    if (reqStatus) {
        console.log("sending message ", data);
        const bData = stream_1.Readable.from(Buffer.from(data, "utf8"));
        // const bData = createReadStream("./llama.png")
        await (0, messages_1.sendMsg)(writeEnc, waitForEncData, bData);
    }
    else {
        console.log("validation failed");
    }
    console.log("done");
}
main();
