/// <reference types="node" />
import { Socket } from "net";
export declare function connect(): Promise<Socket>;
export declare type Context = {
    conn: Socket;
} & Record<any, any>;
export declare const capabilities: string[];
