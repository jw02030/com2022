import { genAndSendSymKey, genIntro, parseIntro, receiveCapabilities, receiveEncCheck, receivePubKey, sendCapabilities, sendEncCheck, sendPubKey } from "./messages"
import { createServer, Socket } from "net"
import { destroyUB, getMessageID, waitForDataUB, waitForEncDataUB, writeEncUB, writeUB, msgListenerUB, registerRecvBuffer } from "./utils"

import { getPublicKey, utils } from "@noble/secp256k1"

import { createCipheriv, createDecipheriv } from "crypto"
import { Duplex, PassThrough, Transform } from "stream"


export async function listen(port = 30522) {
    const server = createServer()
    server.listen(port, () => {
        console.log(`Listening on ${port}`)
    })
    server.on("connection", handleConnection)
}

export const capabilities = [
    "text:0.0.1",
    "sha256:0.0.1",
    "aes-256-ctr:0.0.1",
    "secp256k1:0.0.1"
]



export async function handleConnection(conn: Socket) {
    registerRecvBuffer(conn)
    conn.on("data", (d) => {
        console.log(d)
    })
    const rAddr = `${conn.remoteAddress}:${conn.remotePort}`
    console.log(`New connection from ${rAddr}`)
    conn.setKeepAlive()
    conn.setNoDelay(true)
    conn.on("error", (err) => {
        console.error(`${err} from ${rAddr}`);
        return;
    })
    conn.on("end", () => {
        console.error(`Stream from ${rAddr} closed`)
        return;
    })

    // @ts-ignore
    conn._writableState.highWaterMark = 1;
    // @ts-ignore
    conn._readableState.highWaterMark = 1;

    const destroy = destroyUB.bind(undefined, conn)
    const write = writeUB.bind(undefined, conn)
    const waitForData = waitForDataUB.bind(undefined, conn)

    if (parseIntro(await waitForData())) destroy("Invalid intro")

    await write(genIntro())

    console.log("intro done")

    const clientCaps = await receiveCapabilities(waitForData, destroy)

    // todo: capability negotiation logic

    await sendCapabilities(write, capabilities)

    console.log("client capabilities: ", clientCaps)

    if (getMessageID(await waitForData()) != 1) destroy("No confirmation for capability negotations")

    const privKey = Buffer.from(utils.randomPrivateKey())
    const pubKey = Buffer.from(getPublicKey(privKey))

    // console.log("pubKey: ", pubKey)

    await sendPubKey(write, pubKey)

    const clientPubKey = await receivePubKey(waitForData, destroy)

    // console.log("ClientPubKey: ", clientPubKey)

    await sendEncCheck(write, waitForData, destroy, clientPubKey)

    await receiveEncCheck(write, waitForData, destroy, privKey)

    const { key, iv } = await genAndSendSymKey(write, waitForData, destroy, privKey, clientPubKey)

    const cipher = createCipheriv("aes-256-ctr", key, iv)
    const decipher = createDecipheriv("aes-256-ctr", key, iv)



    const writeEnc = writeEncUB.bind(undefined, conn, cipher)

    const waitForEncData = waitForEncDataUB.bind(undefined, conn, decipher)

    console.log("done")

    // console.log("p2pem:", (await waitForEncData()).toString("utf8"))

    // await writeEnc(genIntro())


    let state = { s: "waiting", wb: new PassThrough() }
    //@ts-ignore
    const msgListener = msgListenerUB.bind(undefined, writeEnc, waitForEncData, destroy, decipher, state, capabilities)
    // now we event-emitter bind to the socket to allow for unprompted comms
    conn.on("data", async (d) => {
        //@ts-ignore
        await msgListener(d)
    })

}

listen()