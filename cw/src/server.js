"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.handleConnection = exports.capabilities = exports.listen = void 0;
const messages_1 = require("./messages");
const net_1 = require("net");
const utils_1 = require("./utils");
const secp256k1_1 = require("@noble/secp256k1");
const crypto_1 = require("crypto");
const stream_1 = require("stream");
async function listen(port = 30522) {
    const server = (0, net_1.createServer)();
    server.listen(port, () => {
        console.log(`Listening on ${port}`);
    });
    server.on("connection", handleConnection);
}
exports.listen = listen;
exports.capabilities = [
    "text:0.0.1",
    "sha256:0.0.1",
    "aes-256-ctr:0.0.1",
    "secp256k1:0.0.1"
];
async function handleConnection(conn) {
    (0, utils_1.registerRecvBuffer)(conn);
    conn.on("data", (d) => {
        console.log(d);
    });
    const rAddr = `${conn.remoteAddress}:${conn.remotePort}`;
    console.log(`New connection from ${rAddr}`);
    conn.setKeepAlive();
    conn.setNoDelay(true);
    conn.on("error", (err) => {
        console.error(`${err} from ${rAddr}`);
        return;
    });
    conn.on("end", () => {
        console.error(`Stream from ${rAddr} closed`);
        return;
    });
    // @ts-ignore
    conn._writableState.highWaterMark = 1;
    // @ts-ignore
    conn._readableState.highWaterMark = 1;
    const destroy = utils_1.destroyUB.bind(undefined, conn);
    const write = utils_1.writeUB.bind(undefined, conn);
    const waitForData = utils_1.waitForDataUB.bind(undefined, conn);
    if ((0, messages_1.parseIntro)(await waitForData()))
        destroy("Invalid intro");
    await write((0, messages_1.genIntro)());
    console.log("intro done");
    const clientCaps = await (0, messages_1.receiveCapabilities)(waitForData, destroy);
    // todo: capability negotiation logic
    await (0, messages_1.sendCapabilities)(write, exports.capabilities);
    console.log("client capabilities: ", clientCaps);
    if ((0, utils_1.getMessageID)(await waitForData()) != 1)
        destroy("No confirmation for capability negotations");
    const privKey = Buffer.from(secp256k1_1.utils.randomPrivateKey());
    const pubKey = Buffer.from((0, secp256k1_1.getPublicKey)(privKey));
    // console.log("pubKey: ", pubKey)
    await (0, messages_1.sendPubKey)(write, pubKey);
    const clientPubKey = await (0, messages_1.receivePubKey)(waitForData, destroy);
    // console.log("ClientPubKey: ", clientPubKey)
    await (0, messages_1.sendEncCheck)(write, waitForData, destroy, clientPubKey);
    await (0, messages_1.receiveEncCheck)(write, waitForData, destroy, privKey);
    const { key, iv } = await (0, messages_1.genAndSendSymKey)(write, waitForData, destroy, privKey, clientPubKey);
    const cipher = (0, crypto_1.createCipheriv)("aes-256-ctr", key, iv);
    const decipher = (0, crypto_1.createDecipheriv)("aes-256-ctr", key, iv);
    const writeEnc = utils_1.writeEncUB.bind(undefined, conn, cipher);
    const waitForEncData = utils_1.waitForEncDataUB.bind(undefined, conn, decipher);
    console.log("done");
    // console.log("p2pem:", (await waitForEncData()).toString("utf8"))
    // await writeEnc(genIntro())
    let state = { s: "waiting", wb: new stream_1.PassThrough() };
    //@ts-ignore
    const msgListener = utils_1.msgListenerUB.bind(undefined, writeEnc, waitForEncData, destroy, decipher, state, exports.capabilities);
    // now we event-emitter bind to the socket to allow for unprompted comms
    conn.on("data", async (d) => {
        //@ts-ignore
        await msgListener(d);
    });
}
exports.handleConnection = handleConnection;
listen();
