
import { Socket } from "net";

import { Cipher, Decipher } from "crypto"
import { utils } from "@noble/secp256k1";


import internal, { Duplex, PassThrough, Transform } from "stream";
import { performance } from "perf_hooks";

export const sleep = (ms: number): Promise<void> => new Promise(resolve => setTimeout(resolve, ms));

export type waitForData = () => Promise<Buffer>
export type write = (d: any) => Promise<unknown>
export type destroy = (message: any) => void



export const destroyUB = (conn: Socket, msg: string) => { conn.destroy(new Error(msg)) }
let sendDelay = 50;

export const writeUB = async (conn: Socket, d: any) => {
    const diff = sendDelay - performance.now()
    if (diff > 0) {
        await sleep(diff)
    }
    sendDelay = performance.now() + 100
    return new Promise((res, rej) => {
        conn.write(d, (err) => {
            conn.emit("drain");
            if (err) rej(err)
            setImmediate(() => res(true))
            // res(true)
        })
    })
}


let readBuffer: Buffer[] = []

export function registerRecvBuffer(conn: Socket) {
    conn.on("data", d => readBuffer.push(d))
}
// let writes = 0;
// let reads = 0;

export const writeEncUB = async (conn: Socket, cipher: Cipher, d: any) => {
    // console.log("enc writes:", ++writes)
    const b = cipher.update(d)
    console.log("enc write: ", b)
    await writeUB(conn, b)
}

export async function waitForDataUB(s: Socket): Promise<Buffer> {
    if (readBuffer.length > 0) {
        return readBuffer.shift() as Buffer
    }
    return new Promise(res =>
        s.once("data", d => {
            readBuffer.shift()
            res(d)
        })
    )
}

export async function waitForEncDataUB(s: Socket, decipher: Decipher) {
    // console.log("enc reads:", ++reads)
    const eData = await waitForDataUB(s)
    console.log("encrypted recv ", eData)
    const decData = decipher.update(eData)
    console.log("decr data ", decData)
    return decData
}

export function getMessageID(b: Buffer) {
    return b.readIntBE(0, 2)
}

export function setMessageID(b: Buffer, id: number) {
    b.writeIntBE(id, 0, 2)
    return b;
}


export async function msgListenerUB(write: write, waitForData: waitForData, destroy: destroy, decipher: Decipher, state: any, capabilities: string[], d: Buffer) {

    // const { write, destroy, waitForData } = f
    // let state = f.state;
    // DO NOT DECRYPT UNLESS NEEDED
    // WILL DESYNC COUNTERS

    if (state.s === "waiting") {
        const data = decipher.update(d)
        if (getMessageID(data) !== 8) destroy("Expected ID 8")
        const capLen = data.readIntBE(2, 1)
        const cap = data.toString("utf8", 3, capLen + 3)
        if (!capabilities.includes(cap)) destroy(`Invalid capability ${cap}`)
        const dataLen = data.readBigUInt64BE(capLen + 3)
        console.log(`Request for ${cap} with size ${dataLen}`)
        const res = setMessageID(Buffer.alloc(3), 9)
        res.writeIntBE(1, 2, 1)
        state.s = "receiving";
        state.rec = 0;
        state.size = dataLen;
        state.wb = new PassThrough()
        console.log(res)
        await write(res)
        return;
    } else if (state.s === "receiving") {
        const data = decipher.update(d)
        if (getMessageID(data) !== 10) destroy("Expected ID 10 (chunk)")
        const chnkSize = data.readBigUInt64BE(2)
        const chnkHash = data.subarray(10, 42)
        const chnkData = data.subarray(42)
        const tstHash = await utils.sha256(chnkData)
        const res = setMessageID(Buffer.alloc(3), 11)
        if (Buffer.compare(chnkHash, tstHash) !== 0) {
            res.writeUIntBE(0, 2, 1)
            await write(res)
            return;
        }
        res.writeUIntBE(1, 2, 1)

        state.wb.write(chnkData)
        state.rec += chnkData.length;
        if (state.rec >= state.size) {
            console.log("Received message:")
            console.log(state.wb.read().toString("utf8"))
            state.s = "waiting"
        }
        console.log({ chnkSize, chnkHash: chnkHash.length, chnkData: chnkData.length })
        await write(res)
    }

}





export class SizeChunker extends Transform {
    protected bytesPassed = 0
    protected currentChunk = -1
    protected lastEmittedChunk: undefined | number = undefined
    protected chunkSize;
    protected flushTail;

    constructor(options: internal.TransformOptions & { chunkSize: number, flushTail: boolean }) {
        super({ ...options, readableObjectMode: true });
        this.chunkSize = options.chunkSize ?? 10_000
        this.flushTail = options.flushTail ?? false
        this.readableObjectMode
        this.once("end", () => {
            if (this.flushTail && (this.lastEmittedChunk !== undefined) && this.bytesPassed > 0) {
                this.emit("chunkEnd", this.currentChunk, () => { return });
            }
        });

    }
    protected finishChunk(done: any): void {
        if (this.listenerCount("chunkEnd") > 0) {
            this.emit("chunkEnd", this.currentChunk, () => {
                this.bytesPassed = 0;
                this.lastEmittedChunk = undefined;
                done();
            });
        } else {
            this.bytesPassed = 0;
            this.lastEmittedChunk = undefined;
            done();
        }
    }

    protected startChunk(done: any): void {
        this.currentChunk++;
        if (this.listenerCount("chunkStart") > 0) {
            this.emit("chunkStart", this.currentChunk, done)
        } else {
            done();
        }
    }

    protected pushData(buf: Buffer): void {
        this.push({
            data: buf,
            id: this.currentChunk
        });

        this.bytesPassed += buf.length;
    };

    protected startIfNeededAndPushData(buf: Buffer): void {
        if (this.lastEmittedChunk != this.currentChunk) {
            this.startChunk(() => {
                this.lastEmittedChunk = this.currentChunk;
                this.pushData(buf);
            })
        } else {
            this.pushData(buf);
        }
    }

    _transform(chunk: any, _encoding: BufferEncoding, done: internal.TransformCallback): void {
        const doTransform = () => {

            const bytesLeave = Math.min(chunk.length, this.chunkSize - this.bytesPassed)
            let remainder;

            if (this.bytesPassed + chunk.length < this.chunkSize) {
                this.startIfNeededAndPushData(chunk);
                done();
            } else {

                remainder = bytesLeave - chunk.length;

                if (remainder === 0) {
                    this.startIfNeededAndPushData(chunk);
                    this.finishChunk(done);
                } else {
                    this.startIfNeededAndPushData(chunk.slice(0, bytesLeave));
                    chunk = chunk.slice(bytesLeave);
                    this.finishChunk(doTransform);
                }
            }

        }

        doTransform();
    }
}
