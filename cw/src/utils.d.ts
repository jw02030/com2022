/// <reference types="node" />
import { Socket } from "net";
import { Cipher, Decipher } from "crypto";
import internal, { Transform } from "stream";
export declare const sleep: (ms: number) => Promise<void>;
export declare type waitForData = () => Promise<Buffer>;
export declare type write = (d: any) => Promise<unknown>;
export declare type destroy = (message: any) => void;
export declare const destroyUB: (conn: Socket, msg: string) => void;
export declare const writeUB: (conn: Socket, d: any) => Promise<unknown>;
export declare function registerRecvBuffer(conn: Socket): void;
export declare const writeEncUB: (conn: Socket, cipher: Cipher, d: any) => Promise<void>;
export declare function waitForDataUB(s: Socket): Promise<Buffer>;
export declare function waitForEncDataUB(s: Socket, decipher: Decipher): Promise<Buffer>;
export declare function getMessageID(b: Buffer): number;
export declare function setMessageID(b: Buffer, id: number): Buffer;
export declare function msgListenerUB(write: write, waitForData: waitForData, destroy: destroy, decipher: Decipher, state: any, capabilities: string[], d: Buffer): Promise<void>;
export declare class SizeChunker extends Transform {
    protected bytesPassed: number;
    protected currentChunk: number;
    protected lastEmittedChunk: undefined | number;
    protected chunkSize: number;
    protected flushTail: boolean;
    constructor(options: internal.TransformOptions & {
        chunkSize: number;
        flushTail: boolean;
    });
    protected finishChunk(done: any): void;
    protected startChunk(done: any): void;
    protected pushData(buf: Buffer): void;
    protected startIfNeededAndPushData(buf: Buffer): void;
    _transform(chunk: any, _encoding: BufferEncoding, done: internal.TransformCallback): void;
}
