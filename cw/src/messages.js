"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.sendMsg = exports.sendMsgReq = exports.sendConfirmation = exports.receiveSymKey = exports.genAndSendSymKey = exports.receiveEncCheckED25519 = exports.sendEncCheckED25519 = exports.receiveEncCheck = exports.sendEncCheck = exports.receivePubKey = exports.sendPubKey = exports.receiveCapabilities = exports.sendCapabilities = exports.parseIntro = exports.genIntro = void 0;
const utils_1 = require("./utils");
const Crypto = __importStar(require("crypto"));
const eciesjs_1 = require("eciesjs");
const crypto_1 = require("crypto");
const secp256k1_1 = require("@noble/secp256k1");
const tweetnacl_1 = __importDefault(require("tweetnacl"));
function genIntro() {
    let t = Buffer.alloc(7);
    t.writeIntBE(0, 0, 2);
    t.write("P2PEM", 2, "utf8");
    console.log("sending intro ");
    return t;
}
exports.genIntro = genIntro;
function parseIntro(b) {
    console.log("received intro: ", b.toString("utf8", 2));
    return (b.readIntBE(0, 2) != 0 || b.toString("utf8", 2) !== "P2PEM");
}
exports.parseIntro = parseIntro;
async function sendCapabilities(write, capabilities) {
    //cap primer
    const capLength = capabilities.length;
    let t = (0, utils_1.setMessageID)(Buffer.alloc(4), 5);
    t.writeUIntBE(capLength, 2, 2);
    await write(t);
    for (let i = 0; i < capLength; i++) {
        let cap = capabilities[i];
        t = (0, utils_1.setMessageID)(Buffer.alloc(2 + 1 + cap.length), 6);
        t.writeIntBE(cap.length, 2, 1);
        t.write(cap, 3, "utf8");
        await write(t);
    }
}
exports.sendCapabilities = sendCapabilities;
async function receiveCapabilities(waitForData, destroy) {
    const capPrimer = await waitForData();
    if ((0, utils_1.getMessageID)(capPrimer) != 5)
        destroy("Invalid cap primer message ID");
    const capLength = capPrimer.readUIntBE(2, 2);
    if (capLength > 1024 || capLength < 0)
        destroy("Invalid capability primer");
    const clientCaps = [];
    for (let i = 0; i < capLength; i++) {
        let d = await waitForData();
        if ((0, utils_1.getMessageID)(d) != 6)
            destroy("Invalid cap message ID");
        const capLength = d.readUIntBE(2, 1);
        const recvCap = d.toString("utf8", 3);
        if (recvCap.length != capLength) {
            console.log(d);
            destroy(`Received capability wrong length ${capLength} - ${recvCap}`);
        }
        clientCaps.push(recvCap);
    }
    return clientCaps;
}
exports.receiveCapabilities = receiveCapabilities;
async function sendPubKey(write, pubKey) {
    let t = (0, utils_1.setMessageID)(Buffer.alloc(pubKey.byteLength + 2), 2);
    pubKey.copy(t, 2);
    await write(t);
}
exports.sendPubKey = sendPubKey;
async function receivePubKey(waitForData, destroy) {
    let data = await waitForData();
    if ((0, utils_1.getMessageID)(data) !== 2)
        destroy("Expected pubKey message");
    // const serverPubKey = data.toString("binary", 2)
    return data.subarray(2);
}
exports.receivePubKey = receivePubKey;
async function sendEncCheck(write, waitForData, destroy, clientPubKey) {
    const byteCount = Crypto.randomInt(1_024, (65_536 - 97));
    const bytes = Crypto.randomBytes(byteCount);
    const message = (0, utils_1.setMessageID)(Buffer.alloc(byteCount + 97 + 2), 3);
    const enc = (0, eciesjs_1.encrypt)(clientPubKey, bytes);
    enc.copy(message, 2);
    await write(message);
    const data = await waitForData();
    if ((0, utils_1.getMessageID)(data) !== 4)
        destroy("Expected message ID 4");
    if (Buffer.compare(bytes, data.subarray(2)) !== 0)
        destroy("Encryption ID check buffer content mismatch");
    await sendConfirmation(write);
}
exports.sendEncCheck = sendEncCheck;
async function receiveEncCheck(write, waitForData, destroy, privKey) {
    let d = await waitForData();
    if ((0, utils_1.getMessageID)(d) !== 3)
        destroy("Expected message ID 3");
    const data = (0, eciesjs_1.decrypt)(privKey, d.subarray(2));
    let msg = (0, utils_1.setMessageID)(Buffer.alloc(data.length + 2), 4);
    data.copy(msg, 2);
    await write(msg);
    d = await waitForData();
    if ((0, utils_1.getMessageID)(d) !== 1)
        destroy("Expected message ID 1");
}
exports.receiveEncCheck = receiveEncCheck;
async function sendEncCheckED25519(write, waitForData, destroy, clientPubKey) {
    const byteCount = Crypto.randomInt(1_024, (65_536 - 97));
    const bytes = Crypto.randomBytes(byteCount);
    const message = (0, utils_1.setMessageID)(Buffer.alloc(byteCount + 97 + 2), 3);
    //const enc = encrypt(clientPubKey, bytes)
    const enc = Buffer.from(tweetnacl_1.default.box.after(message, clientPubKey.subarray(clientPubKey.length - 1, clientPubKey.length), clientPubKey.subarray(0, clientPubKey.length - 1)));
    enc.copy(message, 2);
    await write(message);
    const data = await waitForData();
    if ((0, utils_1.getMessageID)(data) !== 4)
        destroy("Expected message ID 4");
    if (Buffer.compare(bytes, data.subarray(2)) !== 0)
        destroy("Encryption ID check buffer content mismatch");
    await sendConfirmation(write);
}
exports.sendEncCheckED25519 = sendEncCheckED25519;
async function receiveEncCheckED25519(write, waitForData, destroy, privKey) {
    let d = await waitForData();
    if ((0, utils_1.getMessageID)(d) !== 3)
        destroy("Expected message ID 3");
    const data = tweetnacl_1.default.box.open.after(d.subarray(2), privKey.subarray(privKey.length - 1, privKey.length), privKey.subarray(0, privKey.length - 1));
    if (!data) {
        destroy("enc check no data");
        return;
    }
    let msg = (0, utils_1.setMessageID)(Buffer.alloc(data.length + 2), 4);
    Buffer.from(data).copy(msg, 2);
    await write(msg);
    d = await waitForData();
    if ((0, utils_1.getMessageID)(d) !== 1)
        destroy("Expected message ID 1");
}
exports.receiveEncCheckED25519 = receiveEncCheckED25519;
async function genAndSendSymKey(write, waitForData, destroy, privKey, clientPubKey) {
    const key = (0, crypto_1.randomBytes)(32);
    const iv = (0, crypto_1.randomBytes)(16);
    const fullKey = Buffer.alloc(key.length + iv.length);
    key.copy(fullKey, 0);
    iv.copy(fullKey, 32);
    const sig = Buffer.from(await (0, secp256k1_1.sign)(await secp256k1_1.utils.sha256(fullKey), privKey)); // 70 bytes
    const encKey = (0, eciesjs_1.encrypt)(clientPubKey, fullKey); //145 bytes
    let t = (0, utils_1.setMessageID)(Buffer.alloc(2 + 2 + encKey.length + 2 + sig.length), 7);
    t.writeIntBE(encKey.length, 2, 2);
    encKey.copy(t, 4);
    t.writeIntBE(sig.length, encKey.length + 4, 2);
    sig.copy(t, encKey.length + 6);
    console.log({
        keyLength: encKey.length, sigLength: sig.length,
        kfb: encKey.at(0), klb: encKey.at(-1),
        sfb: sig.at(0), slb: sig.at(-1)
    });
    await write(t);
    if ((0, utils_1.getMessageID)(await waitForData()) !== 1)
        destroy("expected confirmation of key validity");
    return { key, iv };
}
exports.genAndSendSymKey = genAndSendSymKey;
async function receiveSymKey(waitForData, destroy, write, privKey, serverPubKey) {
    let d = await waitForData();
    if ((0, utils_1.getMessageID)(d) !== 7)
        destroy("expected ID 7");
    const keyLength = d.readIntBE(2, 2);
    const encKey = d.subarray(4, keyLength + 4);
    const sigLength = d.readIntBE(keyLength + 4, 2);
    const sig = d.subarray(keyLength + 6, keyLength + 6 + sigLength);
    console.log({
        keyLength, sigLength,
        kfb: encKey.at(0), klb: encKey.at(-1),
        sfb: sig.at(0), slb: sig.at(-1)
    });
    const fullKey = (0, eciesjs_1.decrypt)(privKey, encKey);
    if (!(0, secp256k1_1.verify)(sig, await secp256k1_1.utils.sha256(fullKey), serverPubKey))
        destroy("Invalid signature for encrypted symKey");
    await sendConfirmation(write);
    const key = fullKey.subarray(0, 32);
    const iv = fullKey.subarray(32);
    return { key, iv };
}
exports.receiveSymKey = receiveSymKey;
async function sendConfirmation(write) {
    await write((0, utils_1.setMessageID)(Buffer.alloc(2), 1));
}
exports.sendConfirmation = sendConfirmation;
async function sendMsgReq(write, waitForData, capability, size) {
    let m = (0, utils_1.setMessageID)(Buffer.alloc(2 + 1 + capability.length + 8), 8);
    m.writeUIntBE(capability.length, 2, 1);
    m.write(capability, 3, "utf8");
    m.writeBigUInt64BE(size, capability.length + 3);
    // m.writeUBigIntBE(size, capability.length + 2, 8)
    await write(m);
    const res = await waitForData();
    if ((0, utils_1.getMessageID)(res) === 9 && res.readIntBE(2, 1) === 1)
        return true;
    return false;
}
exports.sendMsgReq = sendMsgReq;
async function sendMsg(write, waitForData, data) {
    const ckr = new utils_1.SizeChunker({
        chunkSize: 1_000_000,
        flushTail: true
    });
    data.pipe(ckr);
    for await (const chunk of ckr) {
        const data = chunk.data;
        let m = (0, utils_1.setMessageID)(Buffer.alloc(2 + 8), 10);
        m.writeBigUInt64BE(BigInt(data.length), 2);
        const hash = Buffer.from(await secp256k1_1.utils.sha256(data));
        // hash.copy(m, 10)
        // data.copy(m, m.length)
        m = Buffer.concat([m, hash, data]);
        let i = 0;
        do {
            await write(m);
            const res = await waitForData();
            if ((0, utils_1.getMessageID)(res) !== 11)
                console.warn("Chunk ack bad ID");
            if (res.at(2) === 1)
                i = 4;
            i++;
        } while (i < 3);
    }
    console.log("done");
}
exports.sendMsg = sendMsg;
