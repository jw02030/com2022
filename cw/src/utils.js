"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SizeChunker = exports.msgListenerUB = exports.setMessageID = exports.getMessageID = exports.waitForEncDataUB = exports.waitForDataUB = exports.writeEncUB = exports.registerRecvBuffer = exports.writeUB = exports.destroyUB = exports.sleep = void 0;
const secp256k1_1 = require("@noble/secp256k1");
const stream_1 = require("stream");
const perf_hooks_1 = require("perf_hooks");
const sleep = (ms) => new Promise(resolve => setTimeout(resolve, ms));
exports.sleep = sleep;
const destroyUB = (conn, msg) => { conn.destroy(new Error(msg)); };
exports.destroyUB = destroyUB;
let sendDelay = 50;
const writeUB = async (conn, d) => {
    const diff = sendDelay - perf_hooks_1.performance.now();
    if (diff > 0) {
        await (0, exports.sleep)(diff);
    }
    sendDelay = perf_hooks_1.performance.now() + 100;
    return new Promise((res, rej) => {
        conn.write(d, (err) => {
            conn.emit("drain");
            if (err)
                rej(err);
            setImmediate(() => res(true));
            // res(true)
        });
    });
};
exports.writeUB = writeUB;
let readBuffer = [];
function registerRecvBuffer(conn) {
    conn.on("data", d => readBuffer.push(d));
}
exports.registerRecvBuffer = registerRecvBuffer;
// let writes = 0;
// let reads = 0;
const writeEncUB = async (conn, cipher, d) => {
    // console.log("enc writes:", ++writes)
    await (0, exports.writeUB)(conn, cipher.update(d));
};
exports.writeEncUB = writeEncUB;
async function waitForDataUB(s) {
    if (readBuffer.length > 0) {
        return readBuffer.shift();
    }
    return new Promise(res => s.once("data", d => {
        readBuffer.shift();
        res(d);
    }));
}
exports.waitForDataUB = waitForDataUB;
async function waitForEncDataUB(s, decipher) {
    // console.log("enc reads:", ++reads)
    const eData = await waitForDataUB(s);
    const decData = decipher.update(eData);
    return decData;
}
exports.waitForEncDataUB = waitForEncDataUB;
function getMessageID(b) {
    return b.readIntBE(0, 2);
}
exports.getMessageID = getMessageID;
function setMessageID(b, id) {
    b.writeIntBE(id, 0, 2);
    return b;
}
exports.setMessageID = setMessageID;
async function msgListenerUB(write, waitForData, destroy, decipher, state, capabilities, d) {
    // const { write, destroy, waitForData } = f
    // let state = f.state;
    // DO NOT DECRYPT UNLESS NEEDED
    // WILL DESYNC COUNTERS
    if (state.s === "waiting") {
        const data = decipher.update(d);
        if (getMessageID(data) !== 8)
            destroy("Expected ID 8");
        const capLen = data.readIntBE(2, 1);
        const cap = data.toString("utf8", 3, capLen + 3);
        if (!capabilities.includes(cap))
            destroy(`Invalid capability ${cap}`);
        const dataLen = data.readBigUInt64BE(capLen + 3);
        console.log(`Request for ${cap} with size ${dataLen}`);
        const res = setMessageID(Buffer.alloc(3), 9);
        res.writeIntBE(1, 2, 1);
        state.s = "receiving";
        state.rec = 0;
        state.size = dataLen;
        state.wb = new stream_1.PassThrough();
        console.log(res);
        await write(res);
        return;
    }
    else if (state.s === "receiving") {
        const data = decipher.update(d);
        if (getMessageID(data) !== 10)
            destroy("Expected ID 10 (chunk)");
        const chnkSize = data.readBigUInt64BE(2);
        const chnkHash = data.subarray(10, 42);
        const chnkData = data.subarray(42);
        const tstHash = await secp256k1_1.utils.sha256(chnkData);
        const res = setMessageID(Buffer.alloc(3), 11);
        if (Buffer.compare(chnkHash, tstHash) !== 0) {
            res.writeUIntBE(0, 2, 1);
            await write(res);
            return;
        }
        res.writeUIntBE(1, 2, 1);
        state.wb.write(chnkData);
        state.rec += chnkData.length;
        if (state.rec >= state.size) {
            console.log("Received message:");
            console.log(state.wb.read().toString("utf8"));
            state.s = "waiting";
        }
        console.log({ chnkSize, chnkHash: chnkHash.length, chnkData: chnkData.length });
        await write(res);
    }
}
exports.msgListenerUB = msgListenerUB;
class SizeChunker extends stream_1.Transform {
    bytesPassed = 0;
    currentChunk = -1;
    lastEmittedChunk = undefined;
    chunkSize;
    flushTail;
    constructor(options) {
        super({ ...options, readableObjectMode: true });
        this.chunkSize = options.chunkSize ?? 10_000;
        this.flushTail = options.flushTail ?? false;
        this.readableObjectMode;
        this.once("end", () => {
            if (this.flushTail && (this.lastEmittedChunk !== undefined) && this.bytesPassed > 0) {
                this.emit("chunkEnd", this.currentChunk, () => { return; });
            }
        });
    }
    finishChunk(done) {
        if (this.listenerCount("chunkEnd") > 0) {
            this.emit("chunkEnd", this.currentChunk, () => {
                this.bytesPassed = 0;
                this.lastEmittedChunk = undefined;
                done();
            });
        }
        else {
            this.bytesPassed = 0;
            this.lastEmittedChunk = undefined;
            done();
        }
    }
    startChunk(done) {
        this.currentChunk++;
        if (this.listenerCount("chunkStart") > 0) {
            this.emit("chunkStart", this.currentChunk, done);
        }
        else {
            done();
        }
    }
    pushData(buf) {
        this.push({
            data: buf,
            id: this.currentChunk
        });
        this.bytesPassed += buf.length;
    }
    ;
    startIfNeededAndPushData(buf) {
        if (this.lastEmittedChunk != this.currentChunk) {
            this.startChunk(() => {
                this.lastEmittedChunk = this.currentChunk;
                this.pushData(buf);
            });
        }
        else {
            this.pushData(buf);
        }
    }
    _transform(chunk, _encoding, done) {
        const doTransform = () => {
            const bytesLeave = Math.min(chunk.length, this.chunkSize - this.bytesPassed);
            let remainder;
            if (this.bytesPassed + chunk.length < this.chunkSize) {
                this.startIfNeededAndPushData(chunk);
                done();
            }
            else {
                remainder = bytesLeave - chunk.length;
                if (remainder === 0) {
                    this.startIfNeededAndPushData(chunk);
                    this.finishChunk(done);
                }
                else {
                    this.startIfNeededAndPushData(chunk.slice(0, bytesLeave));
                    chunk = chunk.slice(bytesLeave);
                    this.finishChunk(doTransform);
                }
            }
        };
        doTransform();
    }
}
exports.SizeChunker = SizeChunker;
