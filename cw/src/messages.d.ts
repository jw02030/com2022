/// <reference types="node" />
import { destroy, waitForData, write } from "./utils";
import { Readable } from "stream";
export declare function genIntro(): Buffer;
export declare function parseIntro(b: Buffer): boolean;
export declare function sendCapabilities(write: (d: any) => Promise<unknown>, capabilities: string[]): Promise<void>;
export declare function receiveCapabilities(waitForData: waitForData, destroy: any): Promise<string[]>;
export declare function sendPubKey(write: write, pubKey: Buffer): Promise<void>;
export declare function receivePubKey(waitForData: waitForData, destroy: destroy): Promise<Buffer>;
export declare function sendEncCheck(write: write, waitForData: waitForData, destroy: destroy, clientPubKey: Buffer): Promise<void>;
export declare function receiveEncCheck(write: write, waitForData: waitForData, destroy: destroy, privKey: Buffer): Promise<void>;
export declare function sendEncCheckED25519(write: write, waitForData: waitForData, destroy: destroy, clientPubKey: Buffer): Promise<void>;
export declare function receiveEncCheckED25519(write: write, waitForData: waitForData, destroy: destroy, privKey: Buffer): Promise<void>;
export declare function genAndSendSymKey(write: write, waitForData: waitForData, destroy: destroy, privKey: Buffer, clientPubKey: Buffer): Promise<{
    key: Buffer;
    iv: Buffer;
}>;
export declare function receiveSymKey(waitForData: waitForData, destroy: destroy, write: write, privKey: Buffer, serverPubKey: Buffer): Promise<{
    key: Buffer;
    iv: Buffer;
}>;
export declare function sendConfirmation(write: write): Promise<void>;
export declare function sendMsgReq(write: write, waitForData: waitForData, capability: string, size: bigint): Promise<Boolean>;
export declare function sendMsg(write: write, waitForData: waitForData, data: Readable): Promise<void>;
