/// <reference types="node" />
import { Socket } from "net";
export declare function listen(port?: number): Promise<void>;
export declare const capabilities: string[];
export declare function handleConnection(conn: Socket): Promise<void>;
